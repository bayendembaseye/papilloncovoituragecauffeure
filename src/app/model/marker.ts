export class MarkerModel{
    public lat: number;
    public lng: number;
    public draggable: boolean;
    public label?: string;
}