import { ClientModel } from './client';
export class Positions {
    public id: number;
    public latitude: number;
    public longitude: number;
    public client_id: number;
    public client: ClientModel;

}