import { Vehicule } from './vehicule';
import { ClientModel } from './client';
import { TypeProposition } from './typeproposition';
import { Direction } from './direction';
export class Proposition {
    public id: number;
    public lieudepart: string;
    public lieuarrive: string;
    public datedepart: Date;
    public montant: number;
    public vehicule_id: number;
    public client_id: number;
    public typeproposition_id: number;
    public latdepart: number;
    public longdepart: number;
    public latarrive: number;
    public longarrive: number;
    public typeproposition: TypeProposition;
    public client: ClientModel;
    public vehicule: Vehicule;
    public etat: boolean;
    public origine?: Direction;
    public destination?: Direction;
    public nbplacedispo: number;
    public status: boolean;

}