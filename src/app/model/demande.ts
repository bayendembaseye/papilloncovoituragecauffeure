import { Direction } from './direction';
import { ClientModel } from './client';
export class Demande {
    public id: number;
    public datedepart: string;
    public adressedepart: string;
    public adressearrive: string;
    public latdepart: number;
    public lngdepart: number;
    public latarrive: number;
    public lngarrive: number;
    public etat: boolean;
    public client_id: number;
    public nbplace: number;
    public origine?: Direction;
    public destination?: Direction;
    public client: ClientModel;
}