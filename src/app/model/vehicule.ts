import { ClientModel } from './client';
import { TypeVehicule } from './typeVehicule';
export class Vehicule {
    public id: number;
    public nbplace: number;
    public nbplacedispo: number;
    public typevehicule_id: number;
    public client_id: number;
    public typevehicule: TypeVehicule;
    public client: ClientModel;
 
}