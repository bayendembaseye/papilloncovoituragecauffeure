import { ClientModel } from './client';
export class Signaler{
    public id: number;
    public contenu: string;
    public adresse: string;
    public client_id: number;
    public updated_at: Date;
    public created_at: Date;
    public client: ClientModel;
    public latitude: number;
    public longitude: number;
}