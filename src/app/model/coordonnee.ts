export class Coordonnee{
    public latdepart: number;
    public lngdepart: number;
    public adressedepart?: string;
    public latarrive: number;
    public lngarrive: number;
    public adressearrive?: string;
}