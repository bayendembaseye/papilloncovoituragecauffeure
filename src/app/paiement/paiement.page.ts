import { Paiement } from './../model/paiement';
import { ClientModel } from './../model/client';
import { Stripe } from '@ionic-native/stripe/ngx';
import { Storage } from '@ionic/storage';
import { Reserver } from './../model/reserver';
import { ClientService } from './../client.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-paiement',
  templateUrl: './paiement.page.html',
  styleUrls: ['./paiement.page.scss'],
})
export class PaiementPage implements OnInit {
paiementForm: FormGroup;
reserverId;
reserver: Reserver;
numeroCarte;
mois;
annee;
cvc;
client_id;
stripe_key = 'pk_test_ty2BHn7VrEUSXxtxUiRReD2300YxPJpD56';
cardDetails;
client: ClientModel;
paiement = new Paiement();
error_messages = {
  numeroCarte: [{ type : 'required', message : 'le numéro de carte est obligatoire'},
                { type : 'pattern', message : 'le numéro de carte doit contenir seulement des nombres'}
      ],
  mois: [{ type : 'required', message : 'le mois  est obligatoire'},
      { type : 'pattern', message : 'le mois doit contenir seulement des nombres'}
],
  annee: [{ type : 'required', message : 'l\'année  est obligatoire'},
      { type : 'pattern', message : 'l\' année doit contenir seulement des nombres'}
],
  cvc: [{ type : 'required', message : 'le CVC  est obligatoire'},
      { type : 'pattern', message : 'le CVC doit contenir seulement des nombres'}
],
 };
 nombreReservation;
  constructor(private activitedRoute: ActivatedRoute, public formBuilder: FormBuilder,
              public api: ClientService, public storage: Storage, public stripe: Stripe,
              public router: Router) { }

  ngOnInit() {
    this.reserverId = this.activitedRoute.snapshot.paramMap.get('idReservation');
    this.paiementForm = this.formBuilder.group({
      montant: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      client_id: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      reserver_id: new FormControl('',  Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      numeroCarte: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      mois: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      annee: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      cvc: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
    });
    this.getClientId();
    this.getReservation();
  }
  getClientId() {
    this.storage.get('id').then((val) => {
      this.client_id = val;
      this.getInfosClient(val);
      this.getNombreDeReservation(val);
    });
  }
  getReservation() {
    this.api.getReservationsById(this.reserverId).subscribe((resp: Reserver) => {
       this.reserver = resp;
    });
  }
  payer() {
      this.stripe.setPublishableKey(this.stripe_key);

      this.cardDetails = {
        number: this.paiementForm.value.numeroCarte,
        expMonth: this.paiementForm.value.mois,
        expYear: this.paiementForm.value.annee,
        cvc: this.paiementForm.value.cvc
      };

      this.stripe.createCardToken(this.cardDetails)
        .then(token => {
          console.log(token);
          this.paiement.token_id = token.id;
          this.paiement.email = this.client.email;
          this.paiement.montant = this.reserver.proposition.montant * this.reserver.nbplaceres;
          if(this.nombreReservation > 5){
            this.paiement.montant = this.paiement.montant - ((this.paiement.montant * 5) / 100);
          }
          this.paiement.client_id = this.client.id;
          this.paiement.reserver_id = this.reserver.id;
          this.makePayment(this.paiement);
        })
        .catch(error => console.error(error));

  }
  makePayment(paiement: Paiement) {
   this.api.validerPaiement(paiement).subscribe((resp: any) => {
     if (resp.message === 'ok') {
        this.router.navigateByUrl('/une-reservations/' + this.paiement.reserver_id);
     } else {

     }
     console.log(resp);
   });


  }
  getInfosClient(id) {
    this.api.getReservationsById(id).subscribe((resp: ClientModel) => {
      this.client = resp;
    });

  }
  getNombreDeReservation(id){
    this.api.NombredeReservation(id).subscribe((resp) => {
      this.nombreReservation = resp;
      //console.log(resp);
    });
  }

}
