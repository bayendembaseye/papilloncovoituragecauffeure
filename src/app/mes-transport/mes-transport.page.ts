import { Storage } from '@ionic/storage';
import { Proposition } from './../model/proposition';
import { ClientService } from './../client.service';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Direction } from '../model/direction';

@Component({
  selector: 'app-mes-transport',
  templateUrl: './mes-transport.page.html',
  styleUrls: ['./mes-transport.page.scss'],
})
export class MesTransportPage implements OnInit {
propositions: Proposition[];
  constructor(private api: ClientService, private storage: Storage, public alertController: AlertController) { }

  ngOnInit() {
    this.getPropositionByChauffeur();
  }
  
  getPropositionByChauffeur() {
    this.storage.get('id').then((val) => {
      this.api.getPropositionByChauffeur(val).subscribe((resp: Proposition[]) => {
        this.propositions = resp;
        for (let index = 0; index < this.propositions.length; index++) {
          const origine = new Direction();
          origine.lat = this.propositions[index].latdepart;
          origine.lng = this.propositions[index].longdepart;
          const destination = new Direction();
          destination.lat =  this.propositions[index].latarrive;
          destination.lng =  this.propositions[index].longarrive;
          this.propositions[index].origine = origine;
          this.propositions[index].destination = destination;
  
        }
      });
    });
  }
  async confirmerTransport(id){
    const alert = await this.alertController.create({
      header: 'Confirmer!',
      message: 'Vous avez déja effectuer le trajet ?',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ok',
          handler: () => {
            this.api.confirmProposition(id).subscribe((resp) => {
              console.log(resp);
            });
          }
        }
      ]
    });

    await alert.present();
   
  }
}
