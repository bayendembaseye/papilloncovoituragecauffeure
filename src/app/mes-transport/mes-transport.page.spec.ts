import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesTransportPage } from './mes-transport.page';

describe('MesTransportPage', () => {
  let component: MesTransportPage;
  let fixture: ComponentFixture<MesTransportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesTransportPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesTransportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
