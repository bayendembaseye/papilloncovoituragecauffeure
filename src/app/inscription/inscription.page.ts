import { ClientService } from './../client.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { from } from 'rxjs';
import { LoadingController, ToastController } from '@ionic/angular';
import { TypeClient } from '../model/typeClient';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {
  inscriptionForm: FormGroup;
  client: any;
  typeClients: TypeClient[];
  error_messages = {
    matricule: [{ type : 'required', message : 'l\'matricule est obligatoire'},
      {type: 'pattern', message: 'le champs doit contenir seulement des lettres et des chiffres'}
    ]

  };

  constructor(public api: ClientService, private storage: Storage, private route: Router,
              public formBuilder: FormBuilder, public loadingController: LoadingController,
              public toastController: ToastController
  ) {
    this.inscriptionForm = this.formBuilder.group({
      matricule: new FormControl('', Validators.compose([
        Validators.required,
       // Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ]))
    });
  }

  ngOnInit() {
    // this.getTypeClient();
  }

  async register() {
    const toast =  await this.toastController.create({
      message: 'Echec de verification',
      duration: 2000
    });
    console.log(this.inscriptionForm.value);
    const loading = await this.loadingController.create({
      message: 'Patientez...'

    });
    await loading.present();
    this.api.getChauffeurByMatricule(this.inscriptionForm.value.matricule)
      .subscribe((res: any) => {
        /*let id = res['id'];
        this.router.navigate(['/detail/'+id]);*/
        loading.dismiss();
        console.log(res.id + 'hh');
        if (res.id != null) {
          this.storage.set('id', res.id);
          this.route.navigateByUrl('/menu/home');
        } else {
          toast.present();
        }
      }, (err) => {
        toast.present();
       // console.log(err);
        loading.dismiss();
      });
  }



}
