import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { Direction } from '../model/direction';

@Component({
  selector: 'app-mes-propositions',
  templateUrl: './mes-propositions.page.html',
  styleUrls: ['./mes-propositions.page.scss'],
})
export class MesPropositionsPage implements OnInit {
  propositions: any;
  constructor(private api: ClientService, private storage: Storage) { }

  ngOnInit() {
    this.mesProposition();
  }
 mesProposition(){
  this.storage.get('id').then( (val) => {
    this.api.uneProposition(val).subscribe((resp) => {
      this.propositions = resp;
      for (let index = 0; index < this.propositions.length; index++) {
        let origine = new Direction();
        origine.lat = this.propositions[index].latdepart;
        origine.lng = this.propositions[index].longdepart;
        let destination = new Direction();
        destination.lat =  this.propositions[index].latarrive;
        destination.lng =  this.propositions[index].longarrive;
        this.propositions[index].origine = origine;
        this.propositions[index].destination = destination;
        
      }
    });
  });

 }

}
