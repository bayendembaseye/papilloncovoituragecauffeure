import { ClientModel } from './../model/client';
import { ClientService } from './../client.service';
import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  id;

pageChauffeur = [{
  title: 'Acceuil',
  url: '/menu/home',
  icon: 'home'
},
    {
      title: 'Profil',
      url: '/menu/profil',
      icon: 'person'
    },
    {
      title: 'Mes Transport',
      url: '/menu/mes-transport',
      icon: 'eye'
    },
    {
      title: 'Demande',
      url: '/menu/liste-demande', 
      icon: 'eye'

    },
    {
      title: 'Historique',
      url: '/menu/historique-chauffeur',
      icon: 'eye'
    },
  {
    title: 'Signaler',
    url: '/menu/signaler',
    icon: 'warning'
  },


];
selectedPath = '';
client = new ClientModel();
  constructor(private router: Router, private storage: Storage,
              private api: ClientService) {
                //this.verifierProfil();
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath =  event.url;
    });
  }


  ngOnInit() {
    
  }
  verifierProfil(){
    this.storage.get('id').then((val) => {
      this.api.getOneClientsAndType(val).subscribe((resp: ClientModel) => {
          this.client = resp;
      });
    });
  
  }
 

}
