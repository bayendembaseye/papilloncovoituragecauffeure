import { Signaler } from './../model/signaler';
import { Proposition } from './../model/proposition';
import { ClientModel } from './../model/client';
import { ClientService } from './../client.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { Component, ViewChild, OnInit, ElementRef , OnDestroy, AfterViewInit } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { Direction } from '../model/direction';


declare var window;
declare var google;
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  height = 0;
  locations: any;
  client_id;
  id;
  backButtonSubscription;
  propositions: Proposition[];
  lat: number;
  lng: number;
  signalers: Signaler[];
  iconeSignal= "assets/icon/signalico.png";
  constructor(public platform: Platform, public router: Router, public storage: Storage, public api: ClientService,
              public geolocation: Geolocation, public alertController: AlertController,
              public nativeGeocoder: NativeGeocoder, public backgroundMode: BackgroundMode,
              public localNotifications: LocalNotifications) {
    console.log(platform.height());
    this.height = platform.height() - 56;
    this.locations = [];
    this.notification();


  }

  ngOnInit(): void {
    this.verifier();
    this.updateCurrentPosition();
    this.id = setInterval(() => {
    this.updateCurrentPosition();
  }, 10000);
  this.getListSignal();
  this.getPropositionByChauffeur();
  }
  ngOnDestroy() {
    if (this.id) {
      clearInterval(this.id);
    }
    this.backButtonSubscription.unsubscribe();
  }

  // fonction qui verifie si l'utilisateur est déja inscrit
  verifier() {
    this.storage.get('id').then((val) => {
      console.log(val + 'jsd');
      if (val === null) {
      this.router.navigateByUrl('/menu/inscription');
     } else {
      this.getInfoClient(val);
      this.client_id = val;
     }
    });
  }

  // recupere la position actuelle de l'utilisateur

 async  updateCurrentPosition() {
    this.geolocation.getCurrentPosition().then(async (resp) => {
      if (this.client_id) {
        this.lat = resp.coords.latitude;
        this.lng = resp.coords.longitude;
        const positionForm = {
            client_id: this.client_id,
            latitude : resp.coords.latitude,
            longitude: resp.coords.longitude
          };
        this.api.createPosition(positionForm).subscribe((respo) => {
          },
          (error) => {
              console.log(error);
          });
       // }
      }
      // resp.coords.longituderesp.coords.latitude
      //
     }).catch((error) => {
       console.log('Error getting de position', error);
     });
   /*  const alert =  await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'tesxt',
      buttons: ['Cancel', 'Open Modal', 'Delete']
    });

     await alert.present();*/
}
getInfoClient(id) {

  this.api.getOneClientsAndType(id).subscribe((resp: ClientModel) => {
   if (resp == null) {
    this.router.navigateByUrl('/menu/inscription');
   }
  });

}
ngAfterViewInit() {
  this.backButtonSubscription = this.platform.backButton.subscribe(() => {
    ( navigator as any).app.exitApp();
  });
}
onMapReady($event) {
  const trafficLayer = new google.maps.TrafficLayer();
  trafficLayer.setMap($event);
  }
  notification() {
    this.storage.get('id').then((val) => {
      this.api.getPropositionByChauffeur(val).subscribe((resp: Proposition[]) => {
        resp.forEach(element => {
          console.log(element.id);
          this.localNotifications.schedule({
            id: element.id,
            text: 'Nouveau trajet',
            // sound:'file://sound.mp3',
            data: { secret: '/menu/liste-proposition' }
          });
        });
      });
    });
  }

  //liste des incidents signaler
  getListSignal(){
    this.api.getSignalerToday().subscribe((resp: Signaler[]) => {
      this.signalers = resp;
    });
  }
  getPropositionByChauffeur() {
    this.storage.get('id').then((val) => {
      this.api.getPropositionByChauffeurDuJour(val).subscribe((resp: Proposition[]) => {
        this.propositions = resp;
        for (let index = 0; index < this.propositions.length; index++) {
          const origine = new Direction();
          origine.lat = this.propositions[index].latdepart;
          origine.lng = this.propositions[index].longdepart;
          const destination = new Direction();
          destination.lat =  this.propositions[index].latarrive;
          destination.lng =  this.propositions[index].longarrive;
          this.propositions[index].origine = origine;
          this.propositions[index].destination = destination;
        }
      });
    });
  }
}
