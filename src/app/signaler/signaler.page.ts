import { Coordonnee } from './../model/coordonnee';
import { Storage } from '@ionic/storage';
import { LoadingController, Platform } from '@ionic/angular';
import { Signaler } from './../model/signaler';
import { ClientService } from './../client.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { MarkerModel } from '../model/marker';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';

@Component({
  selector: 'app-signaler',
  templateUrl: './signaler.page.html',
  styleUrls: ['./signaler.page.scss'],
})
export class SignalerPage implements OnInit {
signalerForm: FormGroup;
signale = new Signaler();
height =0;

coordonnee = new Coordonnee();
m = new MarkerModel();
map: any;
address="";
error_messages = {
  contenu: [{ type : 'required', message : 'le contenu est obligatoire'}
      ],
      adresse: [{ type : 'required', message : 'l\'adresse est obligatoire'}
    ], }
  constructor(private formBuilder: FormBuilder, private api: ClientService,private nativeGeocoder: NativeGeocoder,
    private loader: LoadingController, public storage:Storage,public platform: Platform, public loadingController: LoadingController,
     private geolocation: Geolocation) {

      this.height = platform.height() / 2;
     }

  ngOnInit() {
    this.signalerForm = this.formBuilder.group({
      client_id: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*'),
      ])),
      contenu: new FormControl('', Validators.required),
      adresse: new FormControl('', Validators.required),
      latitude: new FormControl(),
      longitude: new FormControl()
    });
    this.storage.get('id').then( (val) => {
      this.signale.client_id = val;
    });
    this.loadMap();
  }
    async signaler(){
    const loading = await this.loadingController.create({
      message: 'Patientez...'
    });
    await loading.present();
    this.api.createSignaler(this.signale).subscribe((resp) => {
        loading.dismiss();
    },
    (error) => {
      loading.dismiss();
      console.log(error);
    });
  }

  loadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.signale.latitude = resp.coords.latitude ;
      this.signale.longitude = resp.coords.longitude;
      this.m.draggable = true;
      this.getAddressFromCoords(resp.coords.latitude, resp.coords.longitude);
      const pos = {
        lat: resp.coords.latitude,
        lng: resp.coords.longitude
      };
      this.map.addListener('tilesloaded', () => {

        this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng());
      });


    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  getAddressFromCoords(lattitude, longitude) {
    console.log('getAddressFromCoords ' + lattitude + ' ' + longitude);
    const options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };

    this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
      .then((result: NativeGeocoderReverseResult[]) => {
        this.signale.adresse = '';
        const responseAddress = [];
        for (const [key, value] of Object.entries(result[0])) {
          if (value.length > 0) {
          responseAddress.push(value);
          }
        }
        responseAddress.reverse();
        for (const value of responseAddress) {
          this.signale.adresse += value + ', ';
        }
        this.signale.adresse = this.address.slice(0, -2);
        console.log('adresse' + this.address);
      })
      .catch((error: any) => {
        console.log(error);
        this.signale.adresse = 'Addresse non disponible!';
      });
    return this.signale.adresse;

  }
  markerDragEndDepart(md: MarkerModel, $event) {
    console.log('dragEnd', md, $event.coords);
    /*this.md.lat = $event.coords.lat;
    this.md.lng = $event.coords.lng;*/
    this.coordonnee.latdepart =  $event.coords.lat;
    this.coordonnee.lngdepart =  $event.coords.lat;
    
    this.coordonnee.adressedepart = this.getAddressFromCoords( $event.coords.lat, $event.coords.lng);

  }

}
