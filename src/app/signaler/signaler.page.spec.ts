import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignalerPage } from './signaler.page';

describe('SignalerPage', () => {
  let component: SignalerPage;
  let fixture: ComponentFixture<SignalerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignalerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignalerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
