import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReserverPage } from './reserver.page';

describe('ReserverPage', () => {
  let component: ReserverPage;
  let fixture: ComponentFixture<ReserverPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReserverPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReserverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
