import { Paiement } from './../model/paiement';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Reserver } from './../model/reserver';
import { ClientModel } from './../model/client';
import { Storage } from '@ionic/storage';
import { ClientService } from './../client.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { Note } from '../model/note';
import { LatLng, LatLngLiteral } from '@agm/core';

declare var google;

@Component({
  selector: 'app-une-proposition',
  templateUrl: './une-proposition.page.html',
  styleUrls: ['./une-proposition.page.scss'],
})
export class UnePropositionPage implements OnInit {
proposition_id: any;
propositions: any;
client = new ClientModel();
reservers: Reserver[];
lat;
long;
origin: LatLngLiteral;
destination: LatLngLiteral;
client_id;
@ViewChild('myPanel') directionsPanel: ElementRef;
  constructor(private activitedRoute: ActivatedRoute, private api: ClientService,
              private alertController: AlertController, private storage: Storage,
              public toastController: ToastController, public geolocation: Geolocation,
              public loadingController: LoadingController, public router: Router) { }

  ngOnInit() {
    this.getPosition();
    this.verfierprofil();
    this.proposition_id = this.activitedRoute.snapshot.paramMap.get('id');
    this.storage.get('id').then(async (val) => {
      this.client_id = val;
    });
    this.getOneProposition();

  }
  async getOneProposition() {
    const loading = await this.loadingController.create({
      message: 'Patientez...'
    });
    await loading.present();
    await this.api.uneProposition(this.activitedRoute.snapshot.paramMap.get('id')).subscribe((resp: any) => {
      this.propositions = resp;
      this.origin = { lat: resp.latdepart, lng: resp.longdepart};
      //this.origin.lat =  resp.latdepart;
      //this.origin.lng = resp.longdepart;
      //{ lat: resp.latdepart, lng: resp.longdepart};
     // this.destination.lat =  resp.latarrive; //{ lat: resp.latarrive, lng: resp.longarrive};
    //  this.destination.lng =  resp.longarrive;
      this.destination = { lat: resp.latarrive, lng: resp.longarrive};
      this.getClients();
      loading.dismiss();
      console.log(resp);
    },
    (error) => {
      loading.dismiss();
    });
  }
 async reserver(id: number) {
  this.storage.get('id').then(async (val) => {
    console.log(val );
    const alert = await  this.alertController.create({
      header: 'Saisir le nombre de la place',
      inputs: [
        {
          name: 'name6',
          type: 'number',
          min: 1,
          max: 100,
          placeholder: 'saisir le nombre de place '
        },
      ],
      buttons: [
        {
          text: 'Retour',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: async ( data) => {
           const reserverForm = {
            proposition_id: id,
            client_id: val,
            // date: '2019-01-01 10:00:00', // Date.now()
            nbplaceres: data.name6
            };
           if (data.name6 <= this.propositions.nbplacedispo && data.name6 > 0) {
              this.api.createReserver(reserverForm).subscribe((resp: any) => {
                console.log(resp);
                if (resp.message === 'ko') {
                  this.toastNbPlace();
                } else {
                  // this.getOneProposition();
                  this.router.navigateByUrl('/paiement/' + resp.message);
                }
              });
            } else {
              const toast = await this.toastController.create({
                message: 'Nombre de place(s) indisponible',
                duration: 2000
              });
              toast.present();
            }

           console.log('Confirm Ok' + data.name6);
          }
        }
      ]
    });

    await  alert.present();
  });

}
async toastNbPlace() {
  const toast = await this.toastController.create({
    message: 'Nombres de place(s) indisponible.',
    duration: 2000
  });
  toast.present();
}
verfierprofil() {
  this.storage.get('id').then(async (val) => {
    this.api.getOneClientsAndType(val).subscribe((resp: ClientModel) => {
      this.client = resp;
    });
  });

}
getClients() {
  this.api.getClientByproposition(this.proposition_id).subscribe((resp: Reserver[]) => {
    this.reservers = resp;
  });
}
getPosition() {
  this.geolocation.getCurrentPosition().then(async (resp) => {
          this.lat = resp.coords.latitude,
          this.long = resp.coords.longitude;
   }).catch((error) => {
     console.log('Error getting de position', error);
   });

}
getInfoClient(id) {
  this.api.getOneClientsAndType(id).subscribe((resp: ClientModel) => {
    this.client = resp;
    console.log(resp);
  });
}
async createNote(client_code) {
  const alert = await this.alertController.create({
    header: 'Checkbox',
    inputs: [
      {
        name: 'radio1',
        type: 'radio',
        label: '1/5',
        value: 1,
        checked: true
      },
      {
        name: 'radio2',
        type: 'radio',
        label: '2/5',
        value: 2
      },
      {
        name: 'radio3',
        type: 'radio',
        label: '3/5',
        value: 3
      },
      {
        name: 'radio4',
        type: 'radio',
        label: '4/5',
        value: 4
      },
      {
        name: 'radio5',
        type: 'radio',
        label: '5/5',
        value: 5
      },
    ],
    buttons: [
      {
        text: 'Annuler',
        role: 'cancel',
        cssClass: 'secondary',
        handler: () => {
          console.log('Confirm Cancel');
        }
      }, {
        text: 'Ok',
        handler: (data) => {
          this.saveNote(data, client_code);
          console.log('Confirm Ok' + data);
        }
      }
    ]
  });

  await alert.present();
}
async saveNote(note,  chauffeur_id) {
  const loading = await this.loadingController.create({
    message: 'Veuillez Patientez !!!'
  });
  await loading.present();
  const noter = new Note();
  noter.client_id = chauffeur_id;
  noter.evaluateur = this.client_id;
  noter.note = note;
  this.api.createNote(noter).subscribe((resp: Note) => {
    loading.dismiss();
  },
  (error) => {
    loading.dismiss();

  });

}
async createPaiement(montant, nbPlace, client, reservation, nom, prenom){
  montant  = montant * nbPlace;
  const loading = await this.loadingController.create({
    message: 'Veuillez Patientez !!!'
  });
  const alert = await this.alertController.create({
    header: 'Confirmation de Paiement!',
    message: 'Vous confirmez le paiement de '+ prenom +' ' + nom + ' pour le montant de '+ montant +' €',
    buttons: [
      {
        text: 'Annuler',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
         
        }
      }, {
        text: 'Confirmer',
        handler: () => {
          const paiement = new Paiement();
          paiement.montant = montant;
          paiement.client_id = client;
          paiement.reserver_id = reservation;
          this.api.createPaiement(paiement).subscribe(async (resp) => {
            loading.dismiss();
            const toast = await this.toastController.create({
              message: 'Paiement réussi.',
              duration: 2000
            });
            toast.present();
            this.getOneProposition();
          },async (error) =>{
            loading.dismiss();
            const toast = await this.toastController.create({
              message: 'Echec Paiement.',
              duration: 2000
            });
            toast.present();
          });
        }
      }
    ]
  });
  await alert.present();
}
async validerTrajet(id){
  const alert = await this.alertController.create({
    header: 'Confirmer',
    message: 'Est-ce le trajet a été efectué',
    buttons: [
      {
        text: 'Non',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Oui',
        handler: () => {
         this.api.validerTrajet(id).subscribe((resp) => {
            this.getOneProposition();
         });
        }
      }
    ]
  });

  await alert.present();
}
onMapReady($event) {
  let trafficLayer = new google.maps.TrafficLayer();
  trafficLayer.setMap($event);
  }
}
