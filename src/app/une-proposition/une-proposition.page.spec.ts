import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnePropositionPage } from './une-proposition.page';

describe('UnePropositionPage', () => {
  let component: UnePropositionPage;
  let fixture: ComponentFixture<UnePropositionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnePropositionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnePropositionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
