import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListePropositionPage } from './liste-proposition.page';

describe('ListePropositionPage', () => {
  let component: ListePropositionPage;
  let fixture: ComponentFixture<ListePropositionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListePropositionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListePropositionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
