import { ClientService } from './../client.service';
import { Component, ViewChild, OnInit } from '@angular/core';
import { IonInfiniteScroll, LoadingController } from '@ionic/angular';
import { Direction } from '../model/direction';

@Component({
  selector: 'app-liste-proposition',
  templateUrl: './liste-proposition.page.html',
  styleUrls: ['./liste-proposition.page.scss'],
})
export class ListePropositionPage implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  items = [];
  numTimesLeft = 5;
  propositions: any;

  constructor(private  api: ClientService, private loadingController: LoadingController) {
   // this.addMoreItems();
   }

  ngOnInit() {
    this.liste();
  }
  async liste() {
    const loading = await this.loadingController.create({
      message: 'Patientez...'
    });
    await loading.present();
    await this.api.getProposition().subscribe((resp) => {
      console.log(resp);
      this.propositions = resp;
      for (let index = 0; index < this.propositions.length; index++) {
        const origine = new Direction();
        origine.lat = this.propositions[index].latdepart;
        origine.lng = this.propositions[index].longdepart;
        const destination = new Direction();
        destination.lat =  this.propositions[index].latarrive;
        destination.lng =  this.propositions[index].longarrive;
        this.propositions[index].origine = origine;
        this.propositions[index].destination = destination;

      }
      loading.dismiss();
    },
    (error) => {
      loading.dismiss();
      console.log(error);
    });
  }

  loadData(event) {
    /*setTimeout(() => {
      console.log('Done');
      this.addMoreItems();
      this.numTimesLeft -= 1;
      event.target.complete();
    }, 2000);*/
  }

  /*addMoreItems() {
    for (let i=0; i<10; i++)
      this.items.push(i);
  }*/

}
