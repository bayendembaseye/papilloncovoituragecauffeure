import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeDemandePage } from './liste-demande.page';

describe('ListeDemandePage', () => {
  let component: ListeDemandePage;
  let fixture: ComponentFixture<ListeDemandePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeDemandePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeDemandePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
