import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListeDemandePage } from './liste-demande.page';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
const routes: Routes = [
  {
    path: '',
    component: ListeDemandePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgmCoreModule.forRoot({
      //apiKey: 'AIzaSyBb6fFkz58oVRSegXQzxUTlqaBFNN95AA0' //for browser
      //apiKey: 'AIzaSyALSLqyG1ATFGNEOMgGfuLMm2vXTITyqXg' // pour android
     // apiKey: 'AIzaSyAd9YIATdY0H83OpK9Xm_ne6hhA_jMrxHA'
     apiKey: 'AIzaSyDJby-hPhgoq4hIhiwKiHYvYmEUn74qnBw'
     // apiKey: 'AIzaSyAnku-8yhTOiYJ9sn1QF0wAU3Qtvlt6GIM'
    }),
    AgmDirectionModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListeDemandePage]
})
export class ListeDemandePageModule {}
