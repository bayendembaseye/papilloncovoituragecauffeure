import { Storage } from '@ionic/storage';
import { Demande } from './../model/demande';
import { Proposition } from './../model/proposition';
import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { LoadingController, AlertController, ToastController } from '@ionic/angular';
import { Direction } from '../model/direction';

@Component({
  selector: 'app-liste-demande',
  templateUrl: './liste-demande.page.html',
  styleUrls: ['./liste-demande.page.scss'],
})
export class ListeDemandePage implements OnInit {
demandes: Demande[];
idChaffeur;
  constructor(private  api: ClientService, private loadingController: LoadingController,
              private alertController: AlertController, public storage: Storage, public toastController: ToastController) { }

  ngOnInit() {
    this.getDemande();
    this.storage.get('id').then((val) => {
      this.idChaffeur = val;
    });
  }
  async getDemande() {
    const loading = await this.loadingController.create({
      message: 'Patientez...'
    });
    await loading.present();
    this.api.getDemandeNoValidate().subscribe((resp: Demande[]) => {
      this.demandes = resp;
      for (let index = 0; index < this.demandes.length; index++) {
      const origine = new Direction();
      origine.lat = this.demandes[index].latdepart;
      origine.lng = this.demandes[index].lngdepart;
      const destination = new Direction();
      destination.lat =  this.demandes[index].latarrive;
      destination.lng =  this.demandes[index].lngarrive;
      this.demandes[index].origine = origine;
      this.demandes[index].destination = destination;
      }
      loading.dismiss();
    },
    (error) => {
      loading.dismiss();
    });
  }
  async validerDemande(demande: Demande) {
    const alert = await  this.alertController.create({
      header: 'Saisir le montant du trajet',
      inputs: [
        {
          name: 'name6',
          type: 'number',
          min: 1,
          max: 100,
          placeholder: 'saisir le montant du trajet '
        },
      ],
      buttons: [
        {
          text: 'Retour',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: async ( data) => {
            const loading = await this.loadingController.create({
              message: 'Patientez...'
            });
            await loading.present();
            //let  p: any ;
            const p = {
              lieudepart :demande.adressedepart,
              latdepart : demande.latdepart,
              longdepart : demande.lngdepart,
              lieuarrive : demande.adressearrive,
              latarrive : demande.latarrive,
              longarrive : demande.lngdepart,
              nbplace : demande.nbplace,
              datedepart : demande.datedepart,
              demande_id :  demande.id,
             // protected $fillable = ['date','proposition_id','client_id','nbplaceres','etat','paye'];
             //nbplaceres: demande.nbplace,
              montant : data.name6,
              client_id: this.idChaffeur
            }
            console.log(p);
            this.api.validerDemande(p).subscribe( async (resp) => {
            if (resp === 'ok') {
              const toast = await this.toastController.create({
                message: 'La demane a été validé',
                duration: 2000
              });
              loading.dismiss();
              toast.present();
              this.getDemande();

            } else {
              const toast = await this.toastController.create({
                message: 'Nombre de place(s) indisponible pour votre vehicule',
                duration: 2000
              });
              loading.dismiss();
              toast.present();
              this.getDemande();

            }

          }, async (error) => {
            loading.dismiss();
            const toast = await this.toastController.create({
              message: 'Echec veuiller reéssayer',
              duration: 2000
            });
            toast.present();
            this.getDemande();
          });

        }
      }
      ]
    });

    await  alert.present();

  }

}
