import { Signaler } from './../model/signaler';
import { ClientService } from './../client.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-liste-incident',
  templateUrl: './liste-incident.page.html',
  styleUrls: ['./liste-incident.page.scss'],
})
export class ListeIncidentPage implements OnInit {
signalers: Signaler[];
  constructor(public api: ClientService) { }

  ngOnInit() {
  }
  listeIncidents(){
    this.api.getIncident().subscribe((resp: Signaler[]) => {
      this.signalers =resp;
    });
  }

}
