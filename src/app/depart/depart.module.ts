import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DepartPage } from './depart.page';
import { AgmCoreModule ,GoogleMapsAPIWrapper} from '@agm/core';
/*const routes: Routes = [
  {
    path: '',
    component: DepartPage
  }
];*/

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgmCoreModule.forRoot({
    apiKey: 'AIzaSyBb6fFkz58oVRSegXQzxUTlqaBFNN95AA0' //for browser
    //apiKey: 'AIzaSyALSLqyG1ATFGNEOMgGfuLMm2vXTITyqXg' // pour android
   })
    //RouterModule.forChild(routes)
  ],
  declarations: [DepartPage],
  entryComponents: [DepartPage],
  exports: [DepartPage],
  providers: [
    GoogleMapsAPIWrapper // <---
  ],

})
export class DepartPageModule {}
