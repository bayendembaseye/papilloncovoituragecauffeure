import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartPage } from './depart.page';

describe('DepartPage', () => {
  let component: DepartPage;
  let fixture: ComponentFixture<DepartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
