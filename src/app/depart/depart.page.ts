import { Coordonnee } from './../model/coordonnee';
import { LocationModel } from './../model/location';
import { MarkerModel } from './../model/marker';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { Platform } from '@ionic/angular';
import { MapsAPILoader, GoogleMapsAPIWrapper } from '@agm/core';


declare var google: any;
@Component({
  selector: 'app-depart',
  templateUrl: './depart.page.html',
  styleUrls: ['./depart.page.scss'],
})
export class DepartPage implements OnInit {
  modalTitle: string;
  modelId:number;
  map: any;
  address:string;
  height = 0;
  lat: number = 51.673858;
  lng: number = 7.815982;
  ma = new MarkerModel();
  md = new MarkerModel();
  geocoder: any;
  location = new LocationModel();
  coordonnee = new Coordonnee();
  constructor(private navParams: NavParams, private modalCtrl: ModalController,
              private geolocation: Geolocation, private nativeGeocoder: NativeGeocoder,public platform: Platform,
              public mapsApiLoader: MapsAPILoader, private wrapper: GoogleMapsAPIWrapper) {
      this.wrapper = wrapper;
      this.mapsApiLoader = mapsApiLoader;
      this.mapsApiLoader.load().then(() => {
        this.geocoder = new google.maps.Geocoder();
      });
      this.height = platform.height() ;
     }

  ngOnInit() {
    this.loadMap();
    console.table(this.navParams);
    this.modelId = this.navParams.data.paramID;
    this.modalTitle = this.navParams.data.paramTitle;
  }
  async closeModal() {
   const onClosedData: string = "Wrapped Up!";
   await this.modalCtrl.dismiss(this.coordonnee);
    // this.getAddressFromCoords();
   // console.log(this.m );
    // await this.modalCtrl.dismiss(this.m);
  }
  loadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat = resp.coords.latitude ;
      this.lng = resp.coords.longitude;
      this.md.lng = resp.coords.longitude;
      this.md.lat = resp.coords.latitude;
      this.ma.lng = resp.coords.longitude;
      this.ma.lat = resp.coords.latitude;
      this.md.draggable = true;
      this.getAddressFromCoords(resp.coords.latitude, resp.coords.longitude);
      const pos = {
        lat: resp.coords.latitude,
        lng: resp.coords.longitude
      };
      this.map.addListener('tilesloaded', () => {

        this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng());
      });


    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  getAddressFromCoords(lattitude, longitude) {
    console.log('getAddressFromCoords ' + lattitude + ' ' + longitude);
    const options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };

    this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
      .then((result: NativeGeocoderReverseResult[]) => {
        this.address = '';
        const responseAddress = [];
        for (const [key, value] of Object.entries(result[0])) {
          if (value.length > 0) {
          responseAddress.push(value);
          }
        }
        responseAddress.reverse();
        for (const value of responseAddress) {
          this.address += value + ', ';
        }
        this.address = this.address.slice(0, -2);
        console.log('adresse' + this.address);
      })
      .catch((error: any) => {
        console.log(error);
        this.address = 'Addresse non disponible!';
      });
    return this.address;

  }
  markerDragEndDepart(md: MarkerModel, $event) {
    console.log('dragEnd', md, $event.coords);
    /*this.md.lat = $event.coords.lat;
    this.md.lng = $event.coords.lng;*/
    this.coordonnee.latdepart =  $event.coords.lat;
    this.coordonnee.lngdepart =  $event.coords.lat;
    
    this.coordonnee.adressedepart = this.getAddressFromCoords( $event.coords.lat, $event.coords.lng);

  }
  markerDragEndArrive(ma: MarkerModel, $event) {
   /* this.ma.lat = $event.coords.lat;
    this.ma.lng = $event.coords.lng;*/
    console.log('dragEnd', ma, $event.coords);
    this.coordonnee.latarrive = $event.coords.lat;
    this.coordonnee.lngarrive =$event.coords.lng;
    this.coordonnee.adressearrive = this.getAddressFromCoords($event.coords.lat, $event.coords.lng);
  }
 /* findAddressByCoordinates() {
    console.log(this.m.lat + 'test');
    this.geocoder.geocode({
      location: {
        lat: this.m.lat,
        lng: this.m.lng
      }
    }, (results, status) => {
      console.log(results, status);
      this.decomposeAddressComponents(results);
    });
  }
  decomposeAddressComponents(addressArray) {
    console.log(addressArray.length + 'qzr');
    if (addressArray.length === 0) { return false; }
    const address = addressArray[0].address_components;

    for (const element of address) {
      if (element.length === 0 && !element.types) { continue; }

      if (element.types.indexOf('street_number') > -1) {
        this.location.address_level_1 = element.long_name;
        continue;
      }
      if (element.types.indexOf('route') > -1) {
        this.location.address_level_1 += ', ' + element.long_name;
        continue;
      }
      if (element.types.indexOf('locality') > -1) {
        this.location.address_level_2 = element.long_name;
        continue;
      }
      if (element.types.indexOf('administrative_area_level_1') > -1) {
        this.location.address_state = element.long_name;
        continue;
      }
      if (element.types.indexOf('country') > -1) {
        this.location.address_country = element.long_name;
        continue;
      }
      if (element.types.indexOf('postal_code') > -1) {
        this.location.address_zip = element.long_name;
        console.log(this.location.address_zip + 'qzr');
        continue;
      }
    }
  }*/
}


