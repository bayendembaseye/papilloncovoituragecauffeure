import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropositionPage } from './proposition.page';

describe('PropositionPage', () => {
  let component: PropositionPage;
  let fixture: ComponentFixture<PropositionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropositionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropositionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
