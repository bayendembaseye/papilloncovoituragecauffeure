import { DepartPageModule } from './../depart/depart.module';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PropositionPage } from './proposition.page';

const routes: Routes = [
  {
    path: '',
    component: PropositionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    DepartPageModule
  ],
  declarations: [PropositionPage]
})
export class PropositionPageModule {}
