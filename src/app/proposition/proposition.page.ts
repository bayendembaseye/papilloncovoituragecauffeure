
import { Storage } from '@ionic/storage';
import { DepartPage } from './../depart/depart.page';
import { TypeVehicule } from './../model/typeVehicule';
import { ClientService } from './../client.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { TypeProposition } from '../model/typeproposition';
import { DatePipe } from '@angular/common';

declare var google;
@Component({
  selector: 'app-proposition',
  templateUrl: './proposition.page.html',
  styleUrls: ['./proposition.page.scss'],
})

export class PropositionPage implements OnInit {
propositionForm: FormGroup;
dataReturned;
typeVehicules: TypeVehicule[];
typepropositions: TypeProposition[];
typevehicules: TypeVehicule;
latdepart;
longdepart;
latarrive;
longarrive;
client;
lieudepart;
lieuarrive;
typeproposition_id;
nbplace = 0;
nbplacedispo = 0;
montant = 0;


error_messages = {
  nbplace: [{ type : 'required', message : 'le champ est obligatoire'},
        {type: 'pattern', message: 'le champs doit contenir seulement des  chiffres'}
      ],
  nbplacedispo: [{ type : 'required', message : 'le champ est obligatoire'},
        {type: 'pattern', message: 'le champs doit contenir seulement des  chiffres'}
      ],
  typevehicule_id: [{ type : 'required', message : 'le champs adresse est obligatoire'},
      {type: 'pattern', message: 'le champs doit contenir seulement des  chiffres'}
    ],
  lieudepart: [{ type : 'required', message : 'le champs est obligatoire'},

  ],
  lieuarrive: [{ type : 'required', message : 'le champs est obligatoire'}
  ],
  datedepart: [{ type : 'required', message : 'le champs est obligatoire'}
  ],

};
  constructor(public formBuilder: FormBuilder, public loadingController: LoadingController,
              private route: Router, private api: ClientService, private modalCtrl: ModalController,
              private clientService: ClientService, private storage: Storage, private datePipe: DatePipe,) {

              }

  ngOnInit() {
    this.getAllTypeProposition();
    this.getAllTypeVehticule();
    this.propositionForm = this.formBuilder.group({
      nbplace: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      nbplacedispo: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*'),
        // Validators.max(this.nbplace)
      ])),
      typevehicule_id: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      client_id: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*')
      ])),
      lieudepart: new FormControl('', Validators.compose([
        Validators.required
      ])),
      lieuarrive: new FormControl('', Validators.compose([
        Validators.required
      ])),
      datedepart: new FormControl('', Validators.compose([
        Validators.required
      ])),
      montant: new FormControl('', Validators.compose([
        Validators.required
      ])),
      typeproposition_id: new FormControl('', Validators.compose([
        Validators.required
      ])),
      latdepart: new FormControl('', Validators.compose([
        Validators.required
      ])),
      longdepart: new FormControl('', Validators.compose([
         Validators.required
      ])),
      latarrive: new FormControl('', Validators.compose([
        Validators.required
      ])),
      longarrive: new FormControl('', Validators.compose([
       Validators.required
      ])),
    });
    this.latdepart = 545646;
    this.longdepart = 545646;
    this.latarrive = 545646;
    this.longarrive = 545646;
    this.storage.get('id').then((val) => {
      console.log(val + 'jsd');
      this.client = val;
    });
  }

  getAllTypeVehticule() {
    this.api.getAllVehicule().subscribe((resp: TypeVehicule[]) => {
      this.typeVehicules = resp;
    });
  }

  getAllTypeProposition() {
    this.api.getAllTypeproposition().subscribe((resp: TypeProposition[]) => {
      this.typepropositions = resp;
      resp.forEach(element => {
        if (element.typeproposition === 'Demande') {
          this.typeproposition_id = element.id;
          console.log( this.typeproposition_id );
        }

      });
    });
  }
   async register() {
    this.propositionForm.value.datedepart = this.datePipe.transform(this.propositionForm.value.datedepart, "yyyy-MM-dd HH:mm")
    console.log( this.propositionForm.value.datedepart + 'test');
    const loading = await this.loadingController.create({
      message: 'Patientez...'

    });
    await loading.present();

    this.api.createProposition(this.propositionForm.value).subscribe((resp) => {

     console.log(this.propositionForm);
     console.log(resp);
     loading.dismiss();
    }, (err) => {
      console.log(err.data);
      loading.dismiss();
    });
    }
  async openStart() {
    const modal = await this.modalCtrl.create({
      component: DepartPage,
      componentProps: {
        paramID: 123,
        paramTitle: 'Test Title'
      }
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        alert('Modal Sent Data :' + dataReturned.data.latdepart);
        this.latdepart = dataReturned.data.latdepart;
        this.longdepart = dataReturned.data.lngdepart;
        this.latarrive = dataReturned.data.latarrive;
        this.longarrive = dataReturned.data.lngarrive;
        this.lieudepart = dataReturned.data.adressedepart;
        this.lieuarrive = dataReturned.data.adressearrive;
      }
    });

    return await modal.present();
  }

  open_end() {

  }
 

}
