import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UneReservationsPage } from './une-reservations.page';

describe('UneReservationsPage', () => {
  let component: UneReservationsPage;
  let fixture: ComponentFixture<UneReservationsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UneReservationsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UneReservationsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
